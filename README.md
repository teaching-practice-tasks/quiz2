## Questions for Endterm Quiz
By topic:
### Exceptions

1. What is the output of the following code?
```python
try:
    print("Hello")
    print(1 / 0)
except ZeroDivisionError:
    print("Divide by zero error")
```
- [ ] Hello
- [ ] Divide by zero error
- [X] Hello \
Divide by zero error
- [ ] None of the above

2. What will be the output of the following code?
```python
try:
    print(x)
except NameError:
    print("Variable x is not defined")
```
- [X] Variable x is not defined
- [ ] None
- [ ] An error message
- [ ] Undefined

3. What will be the output of the following code?
```python
try:
    x = "spam" + 4
except TypeError:
    print("Unsupported operand type")
```
- [ ] spam
- [ ] 4
- [X] Unsupported operand type
- [ ] An error message

4. What will be the output of the following code?
```python
try:
    print("Python"[5])
except IndexError:
    print("Index out of range")
except:
    print("Something went wrong")
```
- [ ] Index out of range
- [X] n
- [ ] Something went wrong
- [ ] None of the above

5. What will be the output of the following code?
```python
try:
    x = 2 + "2"
except TypeError:
    print("TypeError")
except ValueError:
    print("ValueError")
```
- [ ] Unhandled error
- [ ] ValueError
- [X] TypeError
- [ ] None of the above

---

### Libraries, Unit Tests

1. Which of the following unit tests will fail?
```python
import unittest

class TestMathFunctions(unittest.TestCase):
    
    def test_addition(self):
        self.assertEqual(2 + 2, 4)

    def test_subtraction(self):
        self.assertEqual(5 - 2, 4)

    def test_multiplication(self):
        self.assertEqual(3 * 4, 12)

    def test_division(self):
        self.assertEqual(10 / 5, 2)
```
- [ ] test_addition
- [ ] test_division
- [ ] test_multiplication
- [X] test_subtraction

2. What is a unit test?

- [ ] A test that checks the integration between different modules of a system.
- [X] A test that checks the correctness of a single unit of code, such as a function.
- [ ] A test that checks the performance of a system.
- [ ] A test that checks the user interface of a system.

3. Which Python library is used for working with regular expressions?

- [ ] io
- [ ] sys
- [ ] os
- [X] re

4. Which of the following unit tests will fail?

```python
import unittest

class TestStringFunctions(unittest.TestCase):
    
    def test_uppercase(self):
        self.assertEqual("hello".upper(), "HELLO")

    def test_concatenate(self):
        self.assertEqual("hello" + "world", "helloworld")

    def test_reverse(self):
        self.assertEqual("hello"[::-1], "olleh")
```

- [ ] test_uppercase
- [ ] test_concatenate
- [ ] test_reverse
- [X] None of the above

5. Given the following Python code, which uses the unittest library to test a simple add function:

```python
import unittest

def add(a, b):
    return a + b

class TestAdd(unittest.TestCase):
    def test_add(self):
        self.assertEqual(add(2, 3), 5)
        self.assertEqual(add(0, 0), 0)
        self.assertEqual(add(-2, 2), 0)
```

Which of the following statements is true about the `TestAdd` class?

- [ ] It tests the `add` function by comparing its output to a single expected value
- [X] It tests the `add` function by comparing its output to multiple expected values
- [ ] It tests the `add` function by verifying that it raises a specific exception
- [ ] It tests the `add` function by mocking its dependencies

---

### File I/O

1. Which of the following modes should be used to open a file in write mode,
and overwrite its existing content?

- [ ] r
- [X] w
- [ ] a
- [ ] x

2. Which of the following modes should be used to open a file in read mode, 
and raise an error if the file does not exist?

- [X] r
- [ ] w
- [ ] a
- [ ] x

3. Which of the following methods should be used to 
read the entire contents of a file into a single string?

- [X] f.read()
- [ ] f.readLine()
- [ ] f.readLines()
- [ ] f.write()

4. Consider the following code:
```python
with open("file.txt", "w") as f:
    f.write("hello world")
```
Which of the following statements is true if `file.txt` is not present?

- [ ] FileNotFoundError
- [ ] There will be no error but written content won't be saved anywhere.
- [X] A new file will be created and string will be written in that file.
- [ ] None of the above

5. Consider the following code:
```python
with open("file.txt", "r") as f:
    lines = f.readlines()
    for line in lines:
        print(line.strip())
```
Which of the following statements is true if `file.txt` is not present?

- [X] FileNotFoundError
- [ ] There will be no error but `f.readlines()` will return empty list.
- [ ] A new file with that name will be created `f.readlines()` will return empty list.
- [ ] None of the above

---

### Regular Expressions

1. Which of the following modules should be imported
to work with regular expressions in Python?

- [ ] regex
- [X] re
- [ ] regexp
- [ ] None of the above

2. Which of the following regular expressions matches a string that contains one or more digits?

- [ ] \[0-9]
- [X] \[0-9]+
- [ ] \[0-9]*
- [ ] .\*\[0-9]+.*

3. Which of the following regular expressions matches a string that starts with the letter "a" and ends
with the letter "z", with any number of characters in between?

- [ ] a.+z
- [ ] a?z
- [ ] a*z
- [X] a.*z

4. Which of the following regular expressions matches a string that contains any character except for the letter "a"?

- [ ] \[^a]+
- [X] \[^a]*
- [ ] .\*\[^a].*
- [ ] .\*\[^a]*

5. Which of the following regular expressions matches a string that contains the word "hello" at the beginning
of the string, with any number of characters after it?

- [ ] .\*hello.*
- [ ] .*hello$
- [ ] ^hello$
- [X] ^hello.*

---

### Object-Oriented Programming

1. Given the following code, which line of code will create a new instance of the `Person` class?

```python
class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age
```

- [X] person = Person("Alice", 25)
- [ ] Person.new("Alice", 25)
- [ ] person = new Person("Alice", 25)
- [ ] person = Person.create("Alice", 25)

2. Given the following code, which of the following statements is true?

```python
class Animal:
    def make_sound(self):
        pass

class Dog(Animal):
    def make_sound(self):
        return "Woof!"

class Cat(Animal):
    def make_sound(self):
        return "Meow!"
```

- [ ] `Dog` and `Cat` are instances of the `Animal` class
- [X] `Dog` and `Cat` are subclasses of the `Animal` class
- [ ] `Dog` and `Cat` are independent classes
- [ ] `Animal` is a subclass of both `Dog` and `Cat`

3. Given the following code, which line of code will call the make_sound method of the `Dog` object?

```python
class Animal:
    def make_sound(self):
        pass

class Dog(Animal):
    def make_sound(self):
        return "Woof!"

my_dog = Dog()
```

- [ ] `Dog.make_sound()`
- [X] `my_dog.make_sound()`
- [ ] `Animal.make_sound(my_dog)`
- [ ] `my_dog.Animal.make_sound()`

4. Given the following code, which line of code will print `Hello, world!` to the console?

```python
class Greeting:
    def say_hello(self):
        print("Hello, world!")

class Greeter(Greeting):
    pass

my_greeter = Greeter()
```

- [ ] `my_greeter.Greeting.say_hello()`
- [ ] `my_greeter.Greeter.say_hello()`
- [X] `my_greeter.say_hello()`
- [ ] `Greeting.say_hello(my_greeter)`

5. Given the following code, what will be the output when we create a new `Cat` object with
`my_cat = Cat("Fluffy", 3)` and call the introduce method on that object with `my_cat.introduce()`?

- [ ] My name is Fluffy
- [ ] There will be an error because the `Cat` class does not have an `introduce` method
- [ ] There will be an error because the `Cat` class does not call the `__init__` method of the 
- [X] My name is Fluffy and I am 3 years old